package service;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import Pojo.*;
import dao.UserMapper;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private UserMapper userMapper;

	@Override
	public List<User> userList() throws Exception {
		// TODO Auto-generated method stub
		UserExample example=new UserExample();
		List<User> list=userMapper.selectByExample(example);
		System.out.println("123"+list);
		return list;
	}

	@Override
	public User login(User user) throws Exception {
		User user1=userMapper.selectByUser(user);
		return user1;
	}

	@Override
	public void register(User user) {
		// TODO Auto-generated method stub
		userMapper.insert(user);
	}

	@Override
	public User selectByUser(User user) {
		// TODO Auto-generated method stub
		
		User u = userMapper.selectByUser(user);
		return u;
	}

	
}
