package controller;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import Pojo.User;
import service.UserService;

@Controller

public class UserController {

	@Autowired
	private UserService userService;
	
	@RequestMapping("/login")
	public String userList() throws Exception{
		
		
		return "login";
		
	}
	
	@RequestMapping("/toRegister")
	public String toRegister() throws Exception{
		
		return "register";
		
	}
	
	@RequestMapping("/register")
	public String register(HttpServletRequest request) throws Exception{
		
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		user.setType("zuke");
		userService.register(user);
		
		return "login";
		
	}
	
	
	@RequestMapping("/uniqueCheck")
	@ResponseBody
	public boolean uniqueCheck(String username) throws Exception{
		
		Map<String,Boolean> map = new HashMap<>();
		User user = new User();
		user.setUsername(username);
		User u = userService.selectByUser(user);
		map.put("flag", false);
		if(u!=null){
			return false;
		}
		return true;
		
	}
	
	
	@RequestMapping("/logincheck")
	public String login(User user,Model model,HttpSession httpSession) throws Exception{
		
		User user1=userService.login(user);
		
		if(user1!=null){
			httpSession.setAttribute("user", user1);
			if(user1.getType().equals("zuke")){
				return "zuke/main";
			}
			else{
				return "admin/main1";
			}
		}else{
			String error="error";
			model.addAttribute("error", error);
		return "login";
		}
	}
	@RequestMapping("/toindex")
	public String toindex(Model model) throws Exception{
		
		
		return "admin/index";
		}
	}

